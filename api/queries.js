const Pool = require('pg').Pool
// Creating a connection with DB
const pool = new Pool({
  user: process.env.POSTGRES_USER,             // User retrieved from environment (docker-compose.yml) 
  host: 'db',                                  // name of the host from docker-compose.yml
  database: process.env.POSTGRES_DB,           // Database name retrieved from environment (docker-compose.yml)
  password: process.env.POSTGRES_PASSWORD,     // password retrieved from environment (docker-compose.yml)
  port: 5432,                                  // default port
})

// Test request for demonstration purpose
// you can get delete it once you checked everything is functional
const getFoo = (request, response) =>{
    pool.query('SELECT * from foo', (error, results) => {
        if(error){
            throw error
        }
        response.status(200).json(results.rows)
    })
}

// export your requests here to make them
// accessible from server.js
module.exports = {
    getFoo,
}