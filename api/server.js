const express = require('express')
const db = require('./queries')

// Constants
const PORT = 3000
const HOST = '0.0.0.0'

// App
const app = express()

app.use(express.json())
app.use(
  express.urlencoded({
    extended: true,
  })
)

// root route
app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' })
  })

// Test route for demonstration purpose
// you can get delete it once you checked everything is functional
app.get('/foo', db.getFoo)

app.listen(PORT, HOST)
console.log(`Running on http://${HOST}:${PORT}`)
